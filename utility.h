///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    utility.h
/// @version 1.0 - Initial version
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// @author  Kobe Uyeda <kobek@hawaii.edu>
/// @date    17_Feb_2022
///////////////////////////////////////////////////////////////////////////////
//
#include "catDatabase.h"
#include <stdbool.h>

extern bool validIndex(size_t index);
extern bool sameNameInArray(char name[MAX_NAME_LENGTH]);
