///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    updateCats.c
/// @version 1.0 - Initial version
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// @author  Kobe Uyeda <kobek@hawaii.edu>
/// @date    17_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "updateCats.h"
#include "catDatabase.h"
#include "utility.h"
#include <string.h>

int updateCatName(int index, char newName[]){
   if((strlen(newName) == 0 || sameNameInArray(newName)) || validIndex(index) == false || (strlen(newName)+1) > MAX_NAME_LENGTH ){
      return -1;
   }
   strcpy(nameList[index], newName);
   return 1;
}

int fixCat(int index){
   if (validIndex(index) == false){
      return -1;
   }
   isFixedList[index] = true;
   return 1;
}

int updateCatWeight(int index, float newWeight){
   if (validIndex(index) == false || newWeight <= 0){
      return -1;
   }
   weightList[index] = newWeight;
   return 1;
}
