///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    catDatabase.h
/// @version 1.0 - Initial version
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// @author  Kobe Uyeda <kobek@hawaii.edu>
/// @date    17_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdbool.h>

#define MAX_NAME_LENGTH (30)
#define MAX_DATABASE_LENGTH (300)

#ifndef Global
#define Global
enum genderType{UNKNOWN_GENDER, MALE, FEMALE};
enum breedType{UNKOWN_BREED, MAINE_COON, MANX, SHORTHAIR, PERSIAN, SPHYNX};
#endif
extern size_t amountOfCats;

extern char nameList[MAX_DATABASE_LENGTH][MAX_NAME_LENGTH];
extern enum genderType genderList[MAX_DATABASE_LENGTH];
extern enum breedType breedList[MAX_DATABASE_LENGTH];
extern bool isFixedList[MAX_DATABASE_LENGTH];
extern float weightList[MAX_DATABASE_LENGTH];

extern void resetDataBase();
