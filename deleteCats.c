///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    deleteCats.c
/// @version 1.0 - Initial version
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// @author  Kobe Uyeda <kobek@hawaii.edu>
/// @date    17_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "catDatabase.h"
#include "utility.h"
#include <string.h>

void deleteAllCats(){
   resetDataBase();
}

int deleteCat(int index){
   if (validIndex(index) == false){
      return -1;
   }

   for(size_t i = index; i < amountOfCats; i++){
      strcpy(nameList[i], nameList[i+1]);
      genderList[i] = genderList[i+1];
      breedList[i] = breedList[i+1];
      isFixedList[i] = isFixedList[i+1];
      weightList[i] = weightList[i+1];
   }
   amountOfCats--;
   nameList[amountOfCats][0] = '\0';
   genderList[amountOfCats] = 0;
   breedList[amountOfCats] = 0;
   isFixedList[amountOfCats] = false;
   weightList[amountOfCats] = 0.0;
   return 1;
}
