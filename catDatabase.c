///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    catDatabase.c
/// @version 1.0 - Initial version
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// @author  Kobe Uyeda <kobek@hawaii.edu>
/// @date    17_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include <string.h>

#include "catDatabase.h"

size_t amountOfCats = 0;
char nameList[MAX_DATABASE_LENGTH][MAX_NAME_LENGTH];
enum genderType genderList[MAX_DATABASE_LENGTH];
enum breedType breedList[MAX_DATABASE_LENGTH];
bool isFixedList[MAX_DATABASE_LENGTH];
float weightList[MAX_DATABASE_LENGTH];

void resetDataBase(){
   amountOfCats = 0;
   memset(nameList, 0, sizeof(nameList));
   memset(genderList, 0, sizeof(genderList));
   memset(breedList, 0, sizeof(breedList));
   memset(isFixedList, 0, sizeof(isFixedList));
   memset(weightList, 0, sizeof(weightList));
}
