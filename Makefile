###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 05d - Animal Farm 0 - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.0 - Initial version
###
### Build and test a program that explores integers
###
### @author  Kobe Uyeda <kobek@hawaii.edu>
### @date    17_Feb_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################


HOSTNAME=$(shell hostname --short)
$(info $(HOSTNAME))

CC     = gcc
CFLAGS = -g -Wall -Wextra -DHOST=\"$(HOSTNAME)\"

TARGET = animalFarm

all: clearscreen $(TARGET)

.PHONY: clearscreen

clearscreen:
	clear

catDatabase.o: catDatabase.c catDatabase.h
	$(CC) $(CFLAGS) -c catDatabase.c

utility.o: utility.c utility.h
	$(CC) $(CFLAGS) -c utility.c

addCats.o: addCats.c addCats.h catDatabase.h
	$(CC) $(CFLAGS) -c addCats.c

deleteCats.o: deleteCats.c deleteCats.h catDatabase.h
	$(CC) $(CFLAGS) -c deleteCats.c

reportCats.o: reportCats.c reportCats.h catDatabase.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h utility.h catDatabase.h
	$(CC) $(CFLAGS) -c updateCats.c

main.o: main.c addCats.h deleteCats.h reportCats.h updateCats.h catDatabase.h catDatabase.h
	$(CC) $(CFLAGS) -c main.c

animalFarm: main.o addCats.o deleteCats.o reportCats.o updateCats.o utility.o catDatabase.o
	$(CC) $(CFLAGS) -o $(TARGET)  main.o addCats.o deleteCats.o reportCats.o updateCats.o utility.o catDatabase.o

clean:
	rm -f *.o $(TARGET)

