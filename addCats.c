///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    addCats.c
/// @version 1.0 - Initial version
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// @author  Kobe Uyeda <kobek@hawaii.edu>
/// @date    17_Feb_2022
///////////////////////////////////////////////////////////////////////////////

#include "addCats.h"
#include "catDatabase.h"
#include "utility.h"
#include <string.h>

int addCat(char name[], enum genderType gender, enum breedType breed, bool isFixed, float weight){
   if ((((amountOfCats >= MAX_DATABASE_LENGTH || strlen(name) == 0) || (strlen(name)+1) > MAX_NAME_LENGTH) || sameNameInArray(name)) || weight <= 0){
      return -1;
   }
   strcpy(nameList[amountOfCats], name);
   genderList[amountOfCats]          = gender;
   breedList[amountOfCats]           = breed;
   isFixedList[amountOfCats]         = isFixed;
   weightList[amountOfCats]          = weight;
   amountOfCats++;
   return 1;
}

