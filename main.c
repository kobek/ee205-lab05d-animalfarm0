///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab05d - Animal Farm 0 - EE 205 - Spr 2022
///
/// @file    main.c
/// @version 1.0 - Initial version
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// @author  Kobe Uyeda <kobek@hawaii.edu>
/// @date    17_Feb_2022
///////////////////////////////////////////////////////////////////////////////
#include "addCats.h"
#include "deleteCats.h"
#include "reportCats.h"
#include "updateCats.h"

int main() {
   // Generates a few cats in the database
   addCat( "Loki", MALE, PERSIAN, true, 8.5 ) ;
   addCat( "Milo", MALE, MANX, true, 7.0 ) ;
   addCat( "Bella", FEMALE, MAINE_COON, true, 18.2 ) ;
   addCat( "Kali", FEMALE, SHORTHAIR, false, 9.2 ) ;
   addCat( "Trin", FEMALE, MANX, true, 12.2 ) ;
   addCat( "Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0 ) ;
   addCat("asdfasdfasdfasdfasdfasdfasdfasdfasdf", MALE, MANX, false, 9.5); //Should fail not create cat due to name being longer than the max name length
   addCat("asdfasdfasdfasdfasdfasdfasddfd", MALE, MANX, false, 9.5);       // This shoud also fail due to the string terminator making this string go over 30 bytes
   addCat("asdfasdfasdfasdfasdfasdfasddf", MALE, MANX, false, 9.5);        // This shoud pass

   //Prints all the cats there should be around 7
   printAllCats();
   
   int kali = findCat( "Kali" ) ;
   updateCatName( kali, "Chili" ) ; // this should fail
   printCat( kali );
   printf("\n");

   // The Name should change thsi time with the weight being updated and it being fixed
   updateCatName( kali, "Capulet" ) ;
   updateCatWeight( kali, 9.9 ) ;
   fixCat( kali ) ;
   printCat( kali );
   
   printf("\n");
   printAllCats();

   // Delets Kali from the table
   deleteCat(kali);
   printf("\n");
   printAllCats();
   
   deleteAllCats();
   printAllCats();

}
